// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CC_CCSlateWidgetStyle_generated_h
#error "CCSlateWidgetStyle.generated.h already included, missing '#pragma once' in CCSlateWidgetStyle.h"
#endif
#define CC_CCSlateWidgetStyle_generated_h

#define CC_Source_CC_CCSlateWidgetStyle_h_16_GENERATED_BODY \
	friend CC_API class UScriptStruct* Z_Construct_UScriptStruct_FCCSlateStyle(); \
	static class UScriptStruct* StaticStruct();


#define CC_Source_CC_CCSlateWidgetStyle_h_40_RPC_WRAPPERS
#define CC_Source_CC_CCSlateWidgetStyle_h_40_RPC_WRAPPERS_NO_PURE_DECLS
#define CC_Source_CC_CCSlateWidgetStyle_h_40_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesUCCSlateWidgetStyle(); \
	friend CC_API class UClass* Z_Construct_UClass_UCCSlateWidgetStyle(); \
	public: \
	DECLARE_CLASS(UCCSlateWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/CC"), CC_API) \
	DECLARE_SERIALIZER(UCCSlateWidgetStyle) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define CC_Source_CC_CCSlateWidgetStyle_h_40_INCLASS \
	private: \
	static void StaticRegisterNativesUCCSlateWidgetStyle(); \
	friend CC_API class UClass* Z_Construct_UClass_UCCSlateWidgetStyle(); \
	public: \
	DECLARE_CLASS(UCCSlateWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/CC"), CC_API) \
	DECLARE_SERIALIZER(UCCSlateWidgetStyle) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define CC_Source_CC_CCSlateWidgetStyle_h_40_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CC_API UCCSlateWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCCSlateWidgetStyle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CC_API, UCCSlateWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCCSlateWidgetStyle); \
private: \
	/** Private copy-constructor, should never be used */ \
	CC_API UCCSlateWidgetStyle(const UCCSlateWidgetStyle& InCopy); \
public:


#define CC_Source_CC_CCSlateWidgetStyle_h_40_ENHANCED_CONSTRUCTORS \
private: \
	/** Private copy-constructor, should never be used */ \
	CC_API UCCSlateWidgetStyle(const UCCSlateWidgetStyle& InCopy); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CC_API, UCCSlateWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCCSlateWidgetStyle); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCCSlateWidgetStyle)


#define CC_Source_CC_CCSlateWidgetStyle_h_38_PROLOG
#define CC_Source_CC_CCSlateWidgetStyle_h_40_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CC_Source_CC_CCSlateWidgetStyle_h_40_RPC_WRAPPERS \
	CC_Source_CC_CCSlateWidgetStyle_h_40_INCLASS \
	CC_Source_CC_CCSlateWidgetStyle_h_40_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CC_Source_CC_CCSlateWidgetStyle_h_40_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CC_Source_CC_CCSlateWidgetStyle_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
	CC_Source_CC_CCSlateWidgetStyle_h_40_INCLASS_NO_PURE_DECLS \
	CC_Source_CC_CCSlateWidgetStyle_h_40_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CC_Source_CC_CCSlateWidgetStyle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
