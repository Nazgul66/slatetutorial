// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CC_CCGameMode_generated_h
#error "CCGameMode.generated.h already included, missing '#pragma once' in CCGameMode.h"
#endif
#define CC_CCGameMode_generated_h

#define CC_Source_CC_CCGameMode_h_14_RPC_WRAPPERS
#define CC_Source_CC_CCGameMode_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define CC_Source_CC_CCGameMode_h_14_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesACCGameMode(); \
	friend CC_API class UClass* Z_Construct_UClass_ACCGameMode(); \
	public: \
	DECLARE_CLASS(ACCGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/CC"), NO_API) \
	DECLARE_SERIALIZER(ACCGameMode) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define CC_Source_CC_CCGameMode_h_14_INCLASS \
	private: \
	static void StaticRegisterNativesACCGameMode(); \
	friend CC_API class UClass* Z_Construct_UClass_ACCGameMode(); \
	public: \
	DECLARE_CLASS(ACCGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/CC"), NO_API) \
	DECLARE_SERIALIZER(ACCGameMode) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define CC_Source_CC_CCGameMode_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACCGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACCGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACCGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACCGameMode); \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API ACCGameMode(const ACCGameMode& InCopy); \
public:


#define CC_Source_CC_CCGameMode_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API ACCGameMode(const ACCGameMode& InCopy); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACCGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACCGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACCGameMode)


#define CC_Source_CC_CCGameMode_h_11_PROLOG
#define CC_Source_CC_CCGameMode_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CC_Source_CC_CCGameMode_h_14_RPC_WRAPPERS \
	CC_Source_CC_CCGameMode_h_14_INCLASS \
	CC_Source_CC_CCGameMode_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CC_Source_CC_CCGameMode_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CC_Source_CC_CCGameMode_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	CC_Source_CC_CCGameMode_h_14_INCLASS_NO_PURE_DECLS \
	CC_Source_CC_CCGameMode_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CC_Source_CC_CCGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
