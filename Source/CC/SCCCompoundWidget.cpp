// Fill out your copyright notice in the Description page of Project Settings.

#include "CC.h"
#include "SCCCompoundWidget.h"
#include "SlateOptMacros.h"
#include "FMenuStyle.h"
#include "CCSlateWidgetStyle.h"
#include "CCHUD.h"

#define LOCTEXT_NAMESPACE "SStandardSlateWidget"
BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCCCompoundWidget::Construct(const FArguments& InArgs){
	OwnerHUD = InArgs._OwnerHUD;
	FMenuStyle::Initialize();
	MenuStyle = &FMenuStyle::Get().GetWidgetStyle<FCCSlateStyle>("MY");

	////////////////////////////////////////////////////////////////////////////////////////////////
	/*If the code below doesn't look like C++ to you it's because it (sort-of) isn't,
	Slate makes extensive use of the C++ Prerocessor(macros) and operator overloading,
	Epic is trying to make our lives easier, look-up the macro/operator definitions to see why.*/
	ChildSlot
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Top)
		[
			SNew(STextBlock)
			.TextStyle(&MenuStyle->MenuTitleStyle)
		.Text(FText::FromString("Main Menu"))
		]
	+ SOverlay::Slot()
		.HAlign(HAlign_Right)
		.VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		[
			SNew(SButton)
			.ButtonStyle(&MenuStyle->MenuButtonStyle)
		.TextStyle(&MenuStyle->MenuButtonTextStyle)
		.Text(FText::FromString("Play Game!"))
		.OnClicked(this, &SCCCompoundWidget::PlayGameClicked)
		]
	+ SVerticalBox::Slot()
		[
			SNew(SButton)
			.ButtonStyle(&MenuStyle->MenuButtonStyle)
		.TextStyle(&MenuStyle->MenuButtonTextStyle)
		.Text(FText::FromString("Quit Game"))
		.OnClicked(this, &SCCCompoundWidget::QuitGameClicked)
		]
		]
		];

}

END_SLATE_FUNCTION_BUILD_OPTIMIZATION
#undef LOCTEXT_NAMESPACE


FReply SCCCompoundWidget::PlayGameClicked(){
	if(GEngine){
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, TEXT("PlayGameClicked"));
	}

	// actually the BlueprintImplementable function of the HUD is not called; uncomment if you want to handle the OnClick via Blueprint
	//MainMenuHUD->PlayGameClicked();
	return FReply::Handled();
}

FReply SCCCompoundWidget::QuitGameClicked(){
	if(GEngine){
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, TEXT("QuitGameClicked"));
	}

	// actually the BlueprintImplementable function of the HUD is not called; uncomment if you want to handle the OnClick via Blueprint
	//MainMenuHUD->QuitGameClicked();
	return FReply::Handled();
}