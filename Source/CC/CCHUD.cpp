// Fill out your copyright notice in the Description page of Project Settings.

#include "CC.h"
#include "SCCCompoundWidget.h"
#include "CCHUD.h"
#include "Engine.h"

void ACCHUD::PostInitializeComponents(){
	Super::PostInitializeComponents();
}

void ACCHUD::BeginPlay(){
	
	UIWidget = SNew(SCCCompoundWidget).OwnerHUD(this);
	SAssignNew(MainMenuUI, SButton).MainMenuHUD(this);

	if(GEngine->IsValidLowLevel()){
		GEngine->GameViewport->AddViewportWidgetContent(SNew(SWeakWidget).PossiblyNullContent(MainMenuUI.ToSharedRef()));
	}
	GEngine->GameViewport->AddViewportWidgetContent(SNew(SWeakWidget).PossiblyNullContent(UIWidget.ToSharedRef()));
	UIWidget->SetVisibility(EVisibility::Visible);

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	PlayerController->bShowMouseCursor = true;
}


