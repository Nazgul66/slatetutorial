// Fill out your copyright notice in the Description page of Project Settings.

#include "CC.h"
#include "CCSlateWidgetStyle.h"


const FName FCCSlateStyle::TypeName(TEXT("FCCSlateStyle"));

const FCCSlateStyle& FCCSlateStyle::GetDefault(){
	static FCCSlateStyle Default;
	return Default;
}

void FCCSlateStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

const FName FCCSlateStyle::GetTypeName()const{
	static const FName TypeName = TEXT("FCCSlateStyle");
	return TypeName;
}
