// Fill out your copyright notice in the Description page of Project Settings.

#include "CC.h"
#include "FMenuStyle.h"
#include "SlateGameResources.h"

TSharedPtr<class FSlateStyleSet> FMenuStyle::MenuStyleInstance;// = NULL;

void  FMenuStyle::BeginPlay(){
	Initialize();
}

void FMenuStyle::Initialize(){
	if(!MenuStyleInstance.IsValid()){
		MenuStyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*MenuStyleInstance);
	}
}

void FMenuStyle::Shutdown(){
	FSlateStyleRegistry::UnRegisterSlateStyle(*MenuStyleInstance);
	ensure(MenuStyleInstance.IsUnique());
	MenuStyleInstance.Reset();
}


FName FMenuStyle::GetStyleSetName(){
	static FName StyleSetName(TEXT("MenuStyles"));
	return StyleSetName;
}

TSharedPtr<FSlateStyleSet> FMenuStyle::Create(){
	TSharedPtr<FSlateStyleSet> StyleRef = FSlateGameResources::New(FMenuStyle::GetStyleSetName(), "/Game/UI", "/Game/UI");
	return StyleRef;
}

const ISlateStyle& FMenuStyle::Get(){
	return *MenuStyleInstance;
}