// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "SlateBasics.h"
/**
 * 
 */
class CC_API FMenuStyle
{
public:
	void BeginPlay();

	static void Initialize();

	static void Shutdown();

	static const class ISlateStyle& Get();

	static FName GetStyleSetName();

private:
	static TSharedPtr<class FSlateStyleSet> Create();

	static TSharedPtr<class FSlateStyleSet> MenuStyleInstance;
};
