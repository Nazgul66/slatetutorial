// Fill out your copyright notice in the Description page of Project Settings.

#include "CC.h"
#include "FMenuStyle.h"

class FCCGameModule: public FDefaultGameModuleImpl{
	virtual void StartupModule() override{
		FSlateStyleRegistry::UnRegisterSlateStyle(FMenuStyle::GetStyleSetName());
		FMenuStyle::Initialize();
	}

	virtual void ShutdownModule() override{
		FMenuStyle::Shutdown();
	}
};

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CC, "CC" );
