// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/HUD.h"
#include "CCHUD.generated.h"

/**
 *
 */
class SCCCompoundWidget;

UCLASS()
class CC_API ACCHUD: public AHUD{
	GENERATED_BODY()
public:
	ACCHUD();
	TSharedPtr<SCCCompoundWidget> UIWidget;

	virtual void PostInitializeComponents() override;

	void BeginPlay();

};
