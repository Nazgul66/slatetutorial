// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CCHUD.h"
#include "Widgets/SCompoundWidget.h"

/**
 *
 */
class CC_API SCCCompoundWidget: public SCompoundWidget{
public:
	SLATE_BEGIN_ARGS(SCCCompoundWidget){}
	SLATE_ARGUMENT(TWeakObjectPtr<class ACCHUD>, OwnerHUD)
		SLATE_END_ARGS()

		/** Constructs this widget with InArgs */
		void Construct(const FArguments& InArgs);

	TWeakObjectPtr<class ACCHUD> OwnerHUD;

	const struct FCCSlateStyle* MenuStyle;


	UPROPERTY(EditAnywhere, ReadWriteBlueprint, Catego = "Button")
		FReply PlayGameClicked();


	UPROPERTY(EditAnywhere, ReadWriteBlueprint, Catego = "Button")
		FReply QuitGameClicked();
};
