// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "SlateBasics.h"
#include "CCSlateWidgetStyle.generated.h"

/**
 *
 */
USTRUCT()
struct CC_API FCCSlateStyle: public FSlateWidgetStyle{
	GENERATED_USTRUCT_BODY()

	// FSlateWidgetStyle
		virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override;
	static const FCCSlateStyle& GetDefault();

	UPROPERTY(EditAnywhere, Category = "Appearance")
		FButtonStyle MenuButtonStyle;
	UPROPERTY(EditAnywhere, Category = "Appearance")
		FTextBlockStyle MenuButtonTextStyle;
	UPROPERTY(EditAnywhere, Category = "Appearance")
		FTextBlockStyle MenuTitleStyle;
};





/**
 */
UCLASS(hidecategories = Object, MinimalAPI)
class UCCSlateWidgetStyle: public USlateWidgetStyleContainerBase{
	GENERATED_BODY()

public:
	UCCSlateWidgetStyle(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer){}
	/** The actual data describing the widget appearance. */
	UPROPERTY(EditAnywhere, Category = Appearance, meta = (ShowOnlyInnerProperties))
		FCCSlateStyle MenuStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override{
		return static_cast<const struct FSlateWidgetStyle*>(&MenuStyle);
	}
};
